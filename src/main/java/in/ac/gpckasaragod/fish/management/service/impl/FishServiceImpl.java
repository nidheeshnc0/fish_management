/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.fish.management.service.impl;

import in.ac.gpckasaragod.fish.management.service.FishService;
import in.ac.gpckasaragod.fish.management.ui.model.data.FishDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class FishServiceImpl extends ConnectionServiceImpl implements FishService {

    @Override

    public String saveFishDetails(String fishName, String Amount) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO FISH (FISH_NAME,AMOUNT) VALUES "+" ('" + fishName + "','" + Amount + "')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "save failed";
            } else {
                return "Saved successfully";
            }

        } catch (SQLException ex) {
            Logger.getLogger(FishServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "Save failed";
    }

    @Override
    public FishDetails readFishDetails(Integer id) {
        FishDetails fishDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM FISH WHERE ID=" + id;
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                String fishName = resultSet.getString("FISH_NAME");
                Double Amount = resultSet.getDouble("AMOUNT");
                fishDetails = new FishDetails(id, fishName, Amount);

            }
            return fishDetails;

        } catch (SQLException ex) {
            Logger.getLogger(FishServiceImpl.class.getName()).log(Level.SEVERE, null, ex);

        }
        return fishDetails;

    }

    @Override
    public List<FishDetails> getAllFishDetailses() {
        List<FishDetails> FishDetailses = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM FISH";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                int id = resultSet.getInt("ID");
                String fishName = resultSet.getString("FISH_NAME");
                Double Amount = resultSet.getDouble("AMOUNT");
                FishDetails fishDetail = new FishDetails(id, fishName, Amount);
                FishDetailses.add(fishDetail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FishServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return FishDetailses;
    }

    @Override
    public String updateFishDetailsForm(int id, String fishName, String Amount) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE FISH SET FISH_NAME='"+fishName+"',AMOUNT='"+ Amount+"' WHERE ID="+id+"";
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "Updated failed";
            } else {
                return "Updated successfully";
            }

        } catch (SQLException ex) {
            return "Upadte failed";
        }

    }

    @Override
    public String deleteFishDetailsForm(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM FISH WHERE ID=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete != 1) {
                return "save failed";
            } else {
                return "save successfully";

            }
        } catch (SQLException ex) {
            ex.printStackTrace();

            return "Delete failed";

        }

    }

    public String updateFishDetailsForm(String fishName, Double Amount) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
