/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.fish.management.service;

import in.ac.gpckasaragod.fish.management.ui.model.data.CustomerDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface CustomerService {
    public String saveCustomerDetails(String customerName, String contactNo, Integer fishId, Double quantity, Double total);
    public CustomerDetails readFishDetails(Integer id);
    public List<CustomerDetails> getAllCustomerDetailses();
    
}
