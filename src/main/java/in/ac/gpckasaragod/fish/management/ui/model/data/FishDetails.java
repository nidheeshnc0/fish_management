/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.fish.management.ui.model.data;

/**
 *
 * @author student
 */
public class FishDetails {
    private Integer Id;
    private String fishName;
    private Double Amount;

    public FishDetails(Integer Id, String fishName, Double Amount) {
        this.Id = Id;
        this.fishName = fishName;
        this.Amount = Amount;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getFishName() {
        return fishName;
    }

    public void setFishName(String fishName) {
        this.fishName = fishName;
    }

    public Double getAmount() {
        return Amount;
    }

    public void setAmount(Double Amount) {
        this.Amount = Amount;
    }
}