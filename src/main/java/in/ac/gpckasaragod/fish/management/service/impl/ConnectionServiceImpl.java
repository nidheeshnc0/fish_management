
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.fish.management.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author student
 */
public class ConnectionServiceImpl {
    
    String jdbcUrl = "jdbc:mysql://localhost:3306/";
    String databaseName  = "FISH_MANAGEMENT";
    String connectionString = jdbcUrl+databaseName;
    String username = "root";
    String password = "mysql";

public Connection getConnection() throws SQLException{
     return DriverManager.getConnection(connectionString, username, password);
        
        }
}
        