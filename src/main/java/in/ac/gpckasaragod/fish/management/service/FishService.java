/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.fish.management.service;

import in.ac.gpckasaragod.fish.management.ui.model.data.FishDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface FishService {
    public String saveFishDetails(String fishName,String Amount);
   public FishDetails readFishDetails(Integer id);
   public List<FishDetails> getAllFishDetailses();
   public String updateFishDetailsForm(int id, String fishName, String Amount);
   public String deleteFishDetailsForm(Integer id);
}

