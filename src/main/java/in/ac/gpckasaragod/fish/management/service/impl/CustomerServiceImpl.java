/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.fish.management.service.impl;

import in.ac.gpckasaragod.fish.management.service.CustomerService;
import in.ac.gpckasaragod.fish.management.ui.model.data.CustomerDetails;
import in.ac.gpckasaragod.fish.management.ui.model.data.FishDetails;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class CustomerServiceImpl extends ConnectionServiceImpl implements CustomerService {
    
    
    
    
    @Override
    public String saveCustomerDetails(String customerName, String contactNo, Integer fishId, Double quantity, Double total) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO CUSTOMER (CUSTOMER_NAME,CONTACT_NO,FISH_ID,QUANTITY,TOTAL) VALUES "+" ('" + customerName + "','" + contactNo + "','" + fishId + "','" + quantity + "','" + total + "')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "save failed";
            }else {
                return "save successfully";
            }
            
            }catch (SQLException ex) {
                Logger.getLogger(CustomerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
               return "save failed";
            
            }
    
    
    @Override
    public CustomerDetails readFishDetails(Integer id) {
        FishDetails fishDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM CUSTOMER WHERE ID=" + id;
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()) {
                String customerName = resultSet.getString("CUSTOMER_NAME");
                String contactNo = resultSet.getString("CONTACT_NO");
                Integer fishId = resultSet.getInt("FISH_ID");
                Double quantity = resultSet.getDouble("QUANTITY");
                Double total = resultSet.getDouble("TOTAL");
                customerDetails = new CustomerDetails(id, customerName, contactNo, fishId, quantity, total);
            }
            
            return customerDetails;
            
        }catch (SQLException ex) {
            Logger.getLogger(CustomerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return customerDetails;
    }


    @Override
    public List<CustomerDetails> getAllCustomerDetailses() {
        List<CustomerDetails> CustomerDetailses = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM CUSTOMER";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                int id = resultSet.getInt("ID");
                String customerName = resultSet.getString("CUSTOMER_NAME");
                String contactNo = resultSet.getString("CONTACT_NO");
                Integer fishId = resultSet.getInt("FISH_ID");
                Double quantity = resultSet.getDouble("QUANTITY");
                CustomerDetails customerDetail = new CustomerDetails(id, customerName, contactNo, fishId, quantity);
                CustomerDetailses.add(customerDetail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return CustomerDetailses;
    }

public String updateCustomerDetailsForm(int id, String customerName, String contactNo, int fishId, Double quantity) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE CUSTOMER SET CUSTOMER_NAME='"+customerName+"',CONTACT_NO='"+contactNo+"',FISH_ID='"fishId WHERE ID="+id+"";
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "Updated failed";
            } else {
                return "Updated successfully";
            }

        } catch (SQLException ex) {
            return "Upadte failed";
        }

    
}


